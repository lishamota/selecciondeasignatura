﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using MetroFramework;
using System.Data.OleDb;
using System.Configuration;

namespace SeleccionAsignaturas
{
  
    public partial class Form1 : MetroForm
    {
        OleDbConnection connection;
        public Form1()
        {
            InitializeComponent();

        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            Datos data = new Datos();
            DataSet var = new DataSet();
            var = data.GetQuery("SELECT * FROM Seleccion", "Seleccion");
            metroGrid1.DataSource = var;
            metroGrid1.DataMember = "Seleccion";
        }
        private void updateDatabase()
        {
           
        }
    }
    class Datos
    {
        string connectionString = ConfigurationManager.ConnectionStrings["SA"].ConnectionString;
        public DataSet GetQuery(string query, string resultName)
        {
            DataSet ds = new DataSet();
            OleDbConnection databaseConnection = new OleDbConnection(connectionString);

            OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, databaseConnection);

            dataAdapter.Fill(ds, resultName);
            return ds;
        }
    }
}
